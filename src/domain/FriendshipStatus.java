package domain;

public enum FriendshipStatus {
    PENDING,
    APPROVED,
    REJECTED
}
