import UI.UI;
import domain.Friendship;
import domain.Message;
import domain.Tuple;
import domain.User;
import domain.validators.*;
import repository.Repository;
import repository.db.FriendshipDB;
import repository.db.MessageDB;
import repository.db.UserDB;
import repository.file.FriendshipFile;
import repository.file.UserFile;
import repository.memory.InMemoryRepository;
import service.FriendshipService;
import service.MessageService;
import service.Service;
import service.UserService;


/**
 * Main class
 */
public class Main {
    /**
     * Main
     * @param args -
     */
    public static void main(String[] args) {

        Validator<User> userValidator = new UserValidator();
        // Repository<Long, User> userRepository = new InMemoryRepository<>(userValidator);
        // Repository<Long, User> userRepository = new UserFile("data/users.csv", userValidator);
        Repository<Long, User> userRepository = new UserDB("jdbc:postgresql://localhost:5432/SocialNetwork", "postgres", "163202", userValidator);
        // Repository<Long, User> userRepository = new UserDB("jdbc:postgresql://localhost:5432/postgres", "postgres", "password", userValidator);
        UserService userService = new UserService(userRepository);

        Validator<Friendship> friendshipValidator = new FriendshipValidator();
        // Repository<Tuple<Long, Long>, Friendship> friendshipRepository = new InMemoryRepository<>(friendshipValidator);
        // Repository<Tuple<Long, Long>, Friendship> friendshipRepository = new FriendshipFile("data/friendships.csv", friendshipValidator);
        Repository<Tuple<Long, Long>, Friendship> friendshipRepository = new FriendshipDB("jdbc:postgresql://localhost:5432/SocialNetwork", "postgres", "163202", friendshipValidator);
        // Repository<Tuple<Long, Long>, Friendship> friendshipRepository = new FriendshipDB("jdbc:postgresql://localhost:5432/postgres", "postgres", "password", friendshipValidator);
        FriendshipService friendshipService = new FriendshipService(friendshipRepository);

        Validator<Message> messageValidator = new MessageValidator();
        Repository<Long, Message> messageRepository = new MessageDB("jdbc:postgresql://localhost:5432/SocialNetwork", "postgres", "163202", messageValidator);
        // Repository<Long, Message> messageRepository = new MessageDB("jdbc:postgresql://localhost:5432/postgres", "postgres", "password", messageValidator);
        MessageService messageService = new MessageService(messageRepository);

        Service service = new Service(userService, friendshipService, messageService);
        UI ui = new UI(service, System.in, System.out);
        ui.runMenu();
    }
}
