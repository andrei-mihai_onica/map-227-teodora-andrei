package repository.memory;

import domain.Entity;
import domain.validators.Validator;
import repository.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * In Memory Repository Class
 * @param <ID> -
 * @param <E> -
 */
public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID, E> {

    private final Validator<E> validator;
    Map<ID, E> entities;

    /**
     * Constructor with parameters
     * @param validator - validator
     */
    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities = new HashMap<>();
    }

    /**
     * Find entity by id
     * @param id of entity
     * @return entity with id = id
     */
    @Override
    public E findOne(ID id) {
        if (id == null)
            throw new IllegalArgumentException("Entity does not exist!");
        return entities.get(id);
    }

    /**
     * Find all entities
     * @return all entities values
     */
    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    /**
     * Save entity in repository
     * @param entity - entity to be saved
     * @return - entity
     */
    @Override
    public E save(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity must be not null!");
        validator.validate(entity);
        if (entities.get(entity.getId()) != null) {
            return entity;
        } else entities.put(entity.getId(), entity);
        return null;
    }

    /**
     * Delete entity from repository
     * @param id - id of entity
     * @return entity
     */
    @Override
    public E delete(ID id) {
        if (findOne(id) == null)
            throw new IllegalArgumentException("Entity does not exist!");
        E deleted_entity = findOne(id);
        entities.remove(id);
        return deleted_entity;
    }

    /**
     * Update entity in repository
     * @param entity - entity to update
     * @return entity
     */
    @Override
    public E update(ID id, E entity) {

        if (entity == null)
            throw new IllegalArgumentException("Entity must be not null!");
        validator.validate(entity);

        entities.put(id, entity);

        if (entities.get(entity.getId()) != null) {
            entities.put(entity.getId(), entity);
            return null;
        }
        return entity;

    }
}