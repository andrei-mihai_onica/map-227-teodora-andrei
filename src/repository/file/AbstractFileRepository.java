package repository.file;

import domain.Entity;
import domain.validators.Validator;
import repository.memory.InMemoryRepository;

import java.io.*;
import java.util.Arrays;
import java.util.List;

/**
 * Abstract class for Repository
 * @param <ID> -
 * @param <E> -
 */
public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID, E> {
    String fileName;

    /**
     * Constructor with parameters
     * @param fileName - String fileName
     * @param validator - validator
     */
    public AbstractFileRepository(String fileName, Validator<E> validator) {
        super(validator);
        this.fileName = fileName;
        loadData();
    }

    /**
     * Load data from file
     */
    private void loadData() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] args = line.split(";");
                E entity = extractEntity(Arrays.asList(args));
                super.save(entity);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Extract entity from file
     * @param attributes - attributes of the entity
     * @return entity
     */
    public abstract E extractEntity(List<String> attributes);

    /**
     * Create entity as string for file
     * @param entity - entity
     * @return string of entity
     */
    protected abstract String createEntityAsString(E entity);

    /**
     * Save entity in repository
     * @param entity - entity to be saved
     * @return entity
     */
    @Override
    public E save(E entity) {
        E result = super.save(entity);
        saveData();
        return result;
    }

    /**
     * Delete entity from repository
     * @param id - id of entity
     * @return entity
     */
    @Override
    public E delete(ID id) {
        E result = super.delete(id);
        saveData();
        return result;
    }

    /**
     * Update entity from repository
     * @param id
     * @param entity - entity to update
     * @return
     */
    @Override
    public E update(ID id, E entity) {
        E result = super.update(id, entity);
        saveData();
        return result;
    }


    /**
     * Save data in file
     */
    protected void saveData() {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, true))) {

            PrintWriter writer = new PrintWriter(fileName);
            writer.print("");
            writer.close();

            bufferedWriter.write("");
            bufferedWriter.flush();

            String entityAsString;
            for(E entity : super.findAll()){
                entityAsString = createEntityAsString(entity);
                bufferedWriter.write(entityAsString);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

