package repository.file;

import domain.Friendship;
import domain.FriendshipStatus;
import domain.Tuple;
import domain.validators.Validator;

import java.util.List;

/**
 * Friendship File Class
 */
public class FriendshipFile extends AbstractFileRepository<Tuple<Long, Long>, Friendship> {

    /**
     * Constructor with parameters
     *
     * @param fileName  - String
     * @param validator - Validator
     */
    public FriendshipFile(String fileName, Validator<Friendship> validator) {
        super(fileName, validator);
    }

    /**
     * Extract friendship from file
     *
     * @param attributes - attributes of the friendship
     * @return friendship
     */
    @Override
    public Friendship extractEntity(List<String> attributes) {

        Friendship prietenie = new Friendship(attributes.get(2), FriendshipStatus.PENDING);
        prietenie.setId(new Tuple<>(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1))));

        return prietenie;
    }

    /**
     * Create friendship as string for file
     *
     * @param entity - friendship
     * @return string of friendship
     */
    @Override
    protected String createEntityAsString(Friendship entity) {
        return entity.getId().getLeft() + ";" + entity.getId().getRight() + ";" + entity.getDate() + ";" + entity.getStatus().toString();
    }
}
