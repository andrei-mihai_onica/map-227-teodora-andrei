package repository.file;

import domain.User;
import domain.validators.Validator;

import java.util.List;

/**
 * Abstract File Repository Class
 */
public class UserFile extends AbstractFileRepository<Long, User> {

    /**
     * Constructor with parameters
     * @param fileName - String
     * @param validator - Validator
     */
    public UserFile(String fileName, Validator<User> validator) {
        super(fileName, validator);
    }

    /**
     * Extract user from file
     * @param attributes - attributes of the user
     * @return user
     */
    @Override
    public User extractEntity(List<String> attributes) {
        User user = new User(attributes.get(1));
        user.setId(Long.parseLong(attributes.get(0)));

        return user;
    }

    /**
     * Create user as string for file
     * @param entity - user
     * @return string of user
     */
    @Override
    protected String createEntityAsString(User entity) {
        return entity.getId() + ";" + entity.getUsername();
    }
}
