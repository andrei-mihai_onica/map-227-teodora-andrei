package UI;

/**
 * Interface of UI
 */
public interface InterfaceUI {
    /**
     * Run menu
     */
    public void runMenu();
}
